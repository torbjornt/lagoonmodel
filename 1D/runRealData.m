%  running model with measured data

% load data
% clear
load 2012-concurrent_data.mat


%% run model, check for bad results
dt = 1;
tic
[result, opt] = xxl_spacetime_realdata(data,'dt',dt,'dx',50,...
                                'epsilon',2.8,'r',0.001,...
                                'advection','yes','lambda',1,...
                                'hbt',[-.7 .4],'gbt',[-.5 .4]);
toc
if any(any(isnan(result.u)))
    nnan = sum(isnan(result.u(1,:)));
    disp(sprintf('%u/%u',nnan,size(result.u,2)))
    error('NaNs in data')
end
if max(abs(result.eta-result.zeta(end,:))) > 0.2
    warning('eta-zeta difference at opening above 20cm')
end
% %% create/play "movie"
% F = etazetamov(result,dt,200);



%% comparison

% x-position, grid point for comparison. Lagoon length is 1700 m
L = 1700;
x_relative_pos = 900/L;
% number of grid points
J = floor(L/opt.dx);
ref_grid_point = round(x_relative_pos * J);


ddt = 300;

if mod(ddt,dt) == 0
    step = ddt/dt;
    inds = 1:step:size(result.u,2);
else
    inds = ones(length(outsideTime),1);
    for k = 1:length(data.outsideTime)
        [~,inds(k)] = min(abs(result.time - data.outsideTime(k)));
    end
end

modu_time = NaN(size(data.UTime));
for k = 1:length(data.UTime)
    [~,ind] = min(abs(result.time-data.UTime(k)));
    modu_time(k) = result.time(ind);
end
modu    = result.u(ref_grid_point,inds);
modzeta = result.zeta(ref_grid_point,inds);
modeta  = result.eta(inds);
modtime = result.time(inds);

% %
ustat = compareDataModel(modu,modu_time,data.U,data.UTime,...
    'plot','time','output','none','name','u','unit','m/s');
% set(gcf,'unit','normalized','position',[.55 .5 .3 .4])
zstat = compareDataModel(modzeta,modtime,data.meansealevelInside,data.insideTime,...
    'plot','time','output','none','name','zeta','unit','m');
% set(gcf,'unit','normalized','position',[.55 .1 .3 .4])

% %% save files for use elsewhere
% 
% % opt for climate run
% save('../datafiles/opt2205.mat','opt')
% 
% % result for analysis
% save('../datafiles/modresult2205.mat','result')
% 
