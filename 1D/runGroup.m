clear
load opt
opt.dt = 1;
opt.hs = 1.5;
opt.r = 0.0008;
opt.epsilon = 1;
opt.lambda = 0.1;

group_period = 300;
clear o

inds = 3000:3500;

for group_period = [30,45,60,90,120]
o = xxl_spacetime_groups(opt,group_period);
a=o;
start = floor(length(o.time)/3*2);

o.u = o.u(:,start:end);
o.zeta = o.zeta(:,start:end);
o.eta = o.eta(start:end);
o.time = o.time(1:length(o.eta));
o.V = o.V(:,start:end);
o.Hs = o.Hs(start:end);

[f,s] = fftex(o.u(18,:),1/opt.dt);
ml = max(s);
[p,l] = findpeaks(s,'threshold',ml*1e-4);

figure
subplot(211)
loglog(f,s)
hold on
plot(f(l),p,'r.','markersize',15)
xlim([1e-5 1])
for k = 1:length(l)
    text('units','normalized','position',[.1 1-k*0.08],...
        'string',sprintf('Period %u: %5.2f s',k,1/f(l(k))));
end
title(sprintf('Period for Hs: %u s',group_period));
subplot(212)
gp = 18;
title(sprintf('Grid point: %u',gp))
plot(o.time(inds),standardize(o.Hs(inds)));
hold on
plot(o.time(inds),standardize(o.u(gp,inds)),'r','linewidth',2);

end