%% running model
% clear all
dt = 1;
dx = 50;
tic
[result,opt,terms] = xxl_spacetime_model('dt',dt,'dx',dx,...
                                'epsilon',2.8,'r',0.001,...
                                'advection','yes','lambda',1,...
                                'hbt',[-.7 0.4],'gbt',[-.5 0.4],'hs',1.5,'bform','real');
toc
if isnan(result.u(:,end))
    disp('NaNs in data')
end
if max(abs(result.eta-result.zeta(end,:))) > 0.2
    disp('eta-zeta too large')
end                   

if isempty(opt.gbt)
    opt.gbt = [-0.1, 1];
end

gp = round(18/34*size(result.u,1));

figure;
plot(result.time,result.u(gp,:),result.time,result.zeta(gp,:),result.time,result.eta,'linewidth',2)
legend('u','\zeta','\eta')
% title('\epsilon=0, \lambda=1')
% figuresize(20,10,'centimeters')
% saveas(gcf,'e0l1.pdf')

% %% generate animation
% nFrames = 100;
% F = etazetamov(result,dt);
% 
% %% (re)play animation
% figure;
% axes('pos',[0 0 1 1],'visible','off');
% movie(F,10,7)
% 


%%

htf = find(result.eta(1:end-1) < 0.4 & result.eta(2:end) > 0.4);
hte = find(result.eta(1:end-1) > 0.4 & result.eta(2:end) < 0.4);
ht = union(hte,htf);

plot(result.time,result.eta,'g',...
    result.time,standardize(terms.V_w),'k',...
    'linewidth',1)
hold on
pl = plot(result.time(2:end),standardize(terms.V_sld(18,:)),...
    result.time(2:end),-standardize(terms.advection(18,:)),...
    result.time(2:end),-standardize(terms.press_grad(18,:)),...
    result.time(2:end),standardize(terms.volflux(18,:)),...
    'linewidth',1);
ylim([-4,4])

cols = {'b','b','c','r'};
lines  = {'-','--','--','-'};
for p = 1:length(pl)
    set(pl(p),'color',cols{p},'linestyle',lines{p})
end
for k = 1:length(ht)
    line(result.time([ht(k), ht(k)]), get(gca,'ylim'),'color',[.7,.7,.7],'linestyle','--');
end
legend('eta','V_w','V_{sld}','u du/dx','g d\eta/dx','d\Gamma/dx','location','bestoutside')

figuresize(18,7,'centimeters');
saveas(gcf,'terms.pdf')
