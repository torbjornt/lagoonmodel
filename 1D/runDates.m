

tic
[result, opt] = xxl_spacetime_dates('2011-05-01',... % start day 
				    '2011-05-03',... % end day
				    'dt',2,... % timestep (sec)
				    'dx',50,... % grid length (m)
				    'epsilon',2.8,... % wave flux coeff.
				    'r',0.001,...  % friction coeff.
                    'advection','yes',...
				    'lambda',1,...
                    'hbt',[-0.7 0.4],'gbt',[-0.5 0.4]);
toc

% save('articlestuff/twomonthrun.m','result')