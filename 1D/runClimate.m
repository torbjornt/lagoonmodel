% clear 
% close all
% clc
% 
runRealData
% 
% close all
% clear
keep result opt modu modzeta modtime
% load opt2205.mat
% load eradata.mat
eradata = load('eradata2.mat');
TMDdat = load('tpx7mod');

%%
tic;
o = xxl_spacetime_climatology(eradata,TMDdat,opt,1:34);
toc

%%

% %% pick out daily max
% if exist('o','var') && isstruct(o)
%     disp('Using data in workspace')
% else
%     load climaterun_2905_a
% end
% firstday = ceil(o.time(1));
% lastday = floor(o.time(end));
% daily_umax = runningmax(o.u(2,:),o.time);
% daily_zetamax = runningmax(o.zeta(2,:),o.time);
% 
% figure;
% subplot(211)
% plot(firstday-1:lastday,daily_zetamax);
% datetick
% 
% subplot(212)
% plot(firstday-1:lastday,daily_umax);
% datetick


%% save all data
experiment = '20140521-threemonth-constwave';

save(sprintf('../datafiles/OUT/threemonth/climaterun_%s.mat',experiment),'o')

% climatologydata = strcat(experiment,'-ART-climatology.dat');
% realcasedata = strcat(experiment,'-ART-realcase.dat');
% incidentdata = strcat(experiment,'-ART-incidents.dat');


% %% realcasedata
% T = datestr(modtime,'yyyy-mm-dd HH:MM');
% 
% fid = fopen(['../../articlestuff/',realcasedata,'.dat'],'w');
% 
% fields = fieldnames(opt);
% for c = 1:length(fields)-1
%     if isnumeric(opt.(fields{c}))
%         tt = num2str(opt.(fields{c}));
%     else
%         tt = opt.(fields{c});
%     end
%     fprintf(fid,'%%\t%10s\t%-20s\n',fields{c},tt);
% end
% 
% fprintf(fid,'%16s,\t%9s,\t%9s\n','date','u','zeta');
% for k = 1:length(modu);
%     fprintf(fid,'%16s,\t%3.6f,\t%3.6f\n',T(k,:),modu(k),modzeta(k));
% end
% fclose(fid);
% 
% %% climatology
% 
% [umax,t] = resample_TT(o.u,o.time,'day','max');
% dezmax = resample_TT(o.zeta-o.eta,o.time,'day','max');
% 
% T = datestr(t,'yyyy-mm-dd');
% fid = fopen(['../../articlestuff/',climatologydata],'w');
% fprintf(fid,'%16s,\t%9s,\t%9s\n','date','umax','dze');
% for k = 1:length(umax);
%     fprintf(fid,'%16s,\t%3.6f,\t%3.6f\n',T(k,:),umax(k),dezmax(k));
% end
% 
% fclose(fid);
% 
% %% incidents
% 
% filename = '../../data/xx_incidents.dat';
% fid = fopen(filename,'r');
% dat = textscan(fid,'%10s %u %*u','headerlines',1);
% fclose(fid);
% 
% % get dates of incidents
% inci_date = datenum(dat{1},'yyyy-mm-dd');
% % remove dates from 2010
% inci = inci_date(inci_date > t(1)-2);
% 
% for k = 1:length(inci)
%     [~,inci_ind(k)] = min(abs(inci(k)-t));
%     inci_U(k) = umax(inci_ind(k));
% end
% 
% fid = fopen(['../../articlestuff/',incidentdata,'.dat'],'w');
% 
% fprintf(fid,'%s\n','incitime, inciU');
% 
% maxd = t(inci_ind);
% maxu = umax(inci_ind);
% 
% for l = 1:length(maxd)
%     fprintf(fid,'%s,\t%7.5f\n',...
%         datestr(maxd(l),'yyyy-mm-dd'),maxu(l));
% end
% 
% fclose(fid);
% 
% exit