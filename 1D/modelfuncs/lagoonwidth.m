function [B, grid_x] = lagoonwidth(form,J,L,dx)
%    [B, grid_x] = lagoonwidth(FORM, J, L, dx)
% Generates a vector B that defines the width (shape) of the lagoon.
% 
% 
% These cases are defined:
%  -- simple: rectangular, 100m wide
%  -- complex: irregular polygon
%  -- real: more like the true shape
%  -- bump: rectangular, except for one narrowing cosine bump
% 
% Input:
%  -- FORM: string, as defined in the cases above.
%  -- J: Number of grid points in model. 
%  -- L: length of lagoon
%  -- DX: grid cell size
%
% Output:
%  -- B: vector of length 2*J, width of lagoon.
%  -- grid_x: x-position (meters) of grid points for B.
%
% Example:
%    B = lagoonwidth('complex',J,L,dx);
% 
% As the model uses a staggered grid, B will have twice the number of points
% as u/zeta in the model, to eliminate interpolation for B in the model.
% Hence, if J = 50, length(B) = 100.
% 
 
switch form
    case 'simple'
        B = 170;  % width
        B = ones(2.*J,1).*B;
        grid_x = linspace(dx/2,L,2*J);
    case 'complex'
        N = ceil(L);
        B = 170*ones(N,1);
        B(1:floor(N/10))   = B(1:floor(N/10))   - (floor(N/10):-1:1)'.*40/100';
        B(ceil(N*0.2):floor(N*0.4)) = B(ceil(N*0.2):floor(N*0.4)) ...
            - (1:length(B(ceil(N*0.2):floor(N*0.4))))'.*30./length(B(ceil(N*0.2):floor(N*0.4)));
        B(ceil(N*0.4):floor(N*0.6)) = B(ceil(N*0.4):floor(N*0.6)) - ...
            (length(B(ceil(N*0.4):floor(N*0.6))):-1:1)'.*30/length(B(ceil(N*0.4):floor(N*0.6)))';
        B(floor(N*0.7):end) = B(floor(N*0.7):end) - ...
            (1:length(B(floor(N*0.7):end)))'*70/length(B(floor(N*0.7):end));
        grid_x = linspace(dx/2,L,2*J);
        B = interp1(1:N,B,grid_x);
    case 'real'
        pixel_to_meter_factor = 200/92;
        % values in pixels
        map_x = [0 72 139 192 272 345 386 502 624 690 785];
        map_B = [35 46 76 80 95 91 67 80 42 60 43];
        map_x = map_x * pixel_to_meter_factor;
        map_B = map_B * pixel_to_meter_factor;
        grid_x = linspace(dx/2,L,2*J);
        B = interp1(map_x,map_B,grid_x);
    case 'bump'
        N = ceil(L);
        B = 170*ones(N,1);
        bumpint = round(N*.5):round(N*.7);
        bump = cos(2*pi*(0:length(bumpint)-1)/length(bumpint-1)) - 1;
        B(bumpint) = B(bumpint) + 50*bump';
        grid_x = linspace(dx/2,L,2*J);
        B = interp1(1:N,B,grid_x);
    otherwise
        error('No such shape exist, valid options are "simple", "real", "complex" and "bump".')
end
B = B(:);
grid_x = grid_x(:);