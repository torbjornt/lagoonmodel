function [out, opt, terms] = xxl_spacetime_model(varargin)
%
% Xai-Xai lagoon model with space dependency
% 
%   OUT = XXL_SPACETIME_MODEL(VARARGIN);
%
% first dimension of data matrices:  space
% second dimension of data matrices: time
%
% All input arguments are optional, given with PARAMETER,VALUE pairs.
% They are:
%
%  - dx         Grid distance, default 50
%  - dt         Time step, default 2
%  - r          Friction coefficient, default 0.002
%  - epsilon    Coefficient related to wave mass transport, default 1
%  - lambda     Coefficient related to mass flux due to eta-zeta
%               difference: lambda*(eta - zeta). Default 1.
%  - bform      Shape of lagoon, see LAGOONWIDTH.M, default 'simple'
%  - advection  Switch to determine if advection terms should be included in the
%               the momentum equation. Value is 'yes' or 'no', default 'yes'.
%  - Nper       Number of tidal periods to run the model, default 2
%  - href       Reference water level, relative to H_out: if eta > href => inflow.
%               Default -0.5.
%  - hs         Significant wave height [m], used in calculation of wave mass
%               transport. Default 1.5.
%  - period     Wave period [s], used in calculation of wave mass
%               transport. Default 10.
%   - hbt        two-element vector with upper and lower limit for level of
%                inflow (above upper => max inflow, below lower =>
%                zero). Relative to mean level. Default [-0.5 0.3].
%   - gbt        two-element vector with upper and lower limit for level of
%                pressure driven cross reef flow (above upper => max inflow, below lower =>
%                zero). Relative to mean level, default set in XXL_SPACETIME_MAINLOOP
%
% Output is a struct containing the following fields:
%  - u         JxN matrix of the the current speed u
%  - zeta      JxN matrix of the interior surface elevation, zeta
%  - eta       JxN matrix of the exterior surface elevation, eta
%  - B         2*Jx1 vector of lagoon width
%  - grid_x    2*Jx1 vector of x-positions
%  - time      1xN time vector [hours]
%
% Examples:
%     OUT = XXL_SPACETIME_MODEL;
%   Runs model with default parameters, saves result in OUT.
%
%     OUT = XXL_SPACETIME_MODEL('bform','real','dx',25);
%   Use lagoon width that is similar to real lagoon, and change grid
%   distance to 25 m, rather than the default 50.
% 
% See also XXL_SPACETIME_REALDATA, COMPAREDATAMODEL, ETAZETAMOV,
% XXL_SPACETIME_MAINLOOP, WAVEMASSTRANS, 


% START OF INPUT HANDLING
% http://stackoverflow.com/a/2776238/551171
% set default values
opt = struct('dx',        50,        ...
             'dt',        2,         ...
             'r',         0.002,     ...
             'epsilon',   1,         ...
             'lambda',    1,         ...
             'bform',     'simple',  ...
             'advection', 'yes',     ...
             'hbt',       [-0.5 .3], ...
             'gbt',       [-0.1, 1], ...
             'nper',      2,         ...
             'hs',        1.5,       ...
             'period',    10);

% list names of possible input arguments
optionNames = fieldnames(opt);
% number of arguments
nArgs = length(varargin);
% check of prop/value pairs is given
if mod(nArgs,2) ~= 0
    error(['Must have parameter/value pairs, ',...
        'you have used an odd number of arguments.'])
end

% Iterate over varargin, overwriting any default arguments
for pair = reshape(varargin,2,[])
    % convert to lowercase
    inpName = lower(pair{1});
    
    % check if argument exists, overwrite
    if any(strmatch(inpName,optionNames))
        opt.(inpName) = pair{2};
    else
        error('%s is not a recognized parameter',inpName)
    end
end
% END OF INPUT HANDLING

% defining exterior water level - sum of M2 and S2 sine waves
% angular frequencies of M2 and S2
T_M2 = 12.417*3600; % M2 tidal period [hours]
f_M2 = 2.*pi/T_M2; 

% time vector
time = 0:opt.dt:opt.nper*T_M2;

% SSH outside lagoon, displacement from mean (h = H + eta)
eta  = sin(f_M2.*time);


% length of lagoon [m]
L = 1700;        

% number of grid points
J = floor(L/opt.dx);
% number of (new) time points
N = floor(opt.nper.*T_M2/opt.dt);


% width of lagoon
[B grid_x] = lagoonwidth(opt.bform,J,L,opt.dx);

% mean water depth inside
H_in = 2.7;

% depth outside reef, where RBR was placed (same as REALDATA-case).
depth = 17;
 
% % mass transport based on H_s
V = WaveMassTrans(eta,...
                  ones(size(eta))*opt.hs,...
                  ones(size(eta))*opt.period,...
                  depth,...
                  opt.epsilon,...
                  opt.hbt,...
                  [],1);
% V = WaveMassTransG(eta,ones(size(eta))*opt.hs,...
%                   ones(size(eta))*opt.period,...
%                   0,opt.epsilon,opt.hbt);

% initalize matrices for interior water level (zeta) and
% current speed in lagoon (u). zeta(t=0) = 0, u(t=0) = 0.
zeta = zeros(J,N+1);
u    = zeros(J,N+1);


terms = NaN;
% integrate forward
[u, zeta,terms] = xxl_spacetime_mainloop(u,zeta,eta,V,B,...
                                   opt.dx,opt.dt,H_in,...
                                   opt.r,opt.lambda,opt.advection,opt.gbt);

                               
% create time vector for output
datetime = 0:datenum(0,0,0,0,0,opt.dt):N*datenum(0,0,0,0,0,opt.dt);


% Define output struct
out = struct('u',u, ...
             'zeta',zeta, ...
             'eta', eta, ...
             'B', B, ...
             'grid_x',grid_x,...
             'time',datetime*24);
end
