function [out,opt] = xxl_spacetime_dates(startday,stopday,varargin)
%
% Xai-Xai lagoon model with space dependency, for given period in 2011-2012
% defined by STARTDAY and STOPDAY.
% Data used to force the model is from the ERA-Interim reanalysis (wave
% height and period) and the TPX tidal model (sea surface height).
%
%    OUT = XXL_SPACETIME_REALDATA(STARTDAY,STOPDAY,VARARGIN);
%
% STARTDAY and STOPDAY are strings on the form yyyy-mm-dd.
% VARARGIN are optional, PARAMETER,VALUE pairs.
% 
% Example:
%    OUT =  XXL_SPACETIME_REALDATA('2011-10-16','2011-10-20','epsilon',2,'hbt',[-1.2, .4]);
% Runs model for the days from October 16, 2011 to October 20, 2011,
% changing EPSILON to 2 and HBT to [-1.2, 0.4].
% 
% The different parameters available for change are:
%
%  - dx         Grid distance, default 50
%  - dt         Time step, default 2
%  - r          Friction coefficient, default 0.002
%  - epsilon    Coefficient related to wave mass transport, default 1
%  - bform      Shape of lagoon, see LAGOONWIDTH.M, default 'real'
%  - lambda     Coefficient related to mass flux due to eta-zeta
%               difference: lambda*(eta - zeta). Default 1.
%  - advection  Switch to determine if advection terms should be included in the
%               the momentum equation. Value is 'yes' or 'no', default 'yes'.
%  - hs         Significant wave height. By default, model uses Hs from
%               measurements. If a number is specified, use constant wave
%               height. If a vector is specified, it must be the same
%               length as data.outsideHs.
%  - hbt        two-element vector with upper and lower limit for level of
%               inflow (above upper => max inflow, below lower =>
%               zero). Default [-0.5, 0.3].
%  - gbt        two-element vector with upper and lower limit for level of
%               pressure driven cross reef flow (above upper => max inflow, below lower =>
%               zero) Default defined in XXL_SPACETIME_MAINLOOP.
%
% Output is two structs, OUT and OPT, the first containing the model results,
% the second containing the input parameters. OUT contains the following
% fields:
%  - u          matrix of the the current speed u
%  - zeta       matrix of the interior surface elevation, zeta
%  - eta        matrix of the exterior surface elevation, eta
%  - time       vector for the time of u/zeta/eta
%  - B          2*Jx1 vector of lagoon width
%  - grid_x     2*Jx1 vector of x-positions
%
%
% See also XXL_SPACETIME_MODEL, XXL_SPACETIME_MAINLOOP, WAVEMASSTRANS


% START OF INPUT HANDLING
% http://stackoverflow.com/a/2776238/551171
% set default values
opt = struct('dx',        50,        ...
    'dt',        2,         ...
    'r',         0.001,     ...
    'epsilon',   1,         ...
    'lambda',    1,         ...
    'bform',     'real',    ...
    'advection', 'yes',     ...
    'hbt',       [-0.5 .3], ...
    'gbt',       [-0.1, 1]);

% list names of possible input arguments
optionNames = fieldnames(opt);
% number of arguments
nArgs = length(varargin);
% check of prop/value pairs is given
if mod(nArgs,2) ~= 0
    error(['Must have parameter/value pairs, ',...
        'you have used an odd number of arguments.'])
end

% Iterate over varargin, overwriting any default arguments
for pair = reshape(varargin,2,[])
    % convert to lowercase
    inpName = lower(pair{1});

    % check if argument exists, overwrite
    if any(strmatch(inpName,optionNames))
        opt.(inpName) = pair{2};
    else
        warning('%s is not a recognized parameter, ignored',inpName)
    end
end
% END OF INPUT HANDLING
wavedata = load('eradata2.mat');
tidedata = load('tpx7mod');

startday = datenum(startday,'yyyy-mm-dd');
stopday = datenum(stopday,'yyyy-mm-dd')+1;

% allow for date matrix in wavedata struct
if size(wavedata.time,2) == 6
    wavedata.time = datenum(wavedata.time);
end

% length of lagoon
L = 1700;

% number of grid points
J = floor(L/opt.dx);

% width of lagoon
[B, grid_x] = lagoonwidth(opt.bform,J,L,opt.dx);

% mean water depth inside
H_in = 3;
% mean water depth outside
H_out = 17;


% create time vector for model
time = startday:datenum(0,0,0,0,0,opt.dt):stopday;


% initalize matrices for interior water level (zeta) and
% current speed in lagoon (u). zeta(t=0) = 0, u(t=0) = 0.
zeta = zeros(J,length(time));
u    = zeros(J,length(time));

% create datenum vector of era-time matrix
wavetime = datenum(wavedata.time);


% find indices for start and end of day in data
wstart = find(wavetime <= startday,1,'last');
wstop  = find(wavetime >= stopday,1,'first');
tstart = find(tidedata.SerialDay <= startday,1,'last');
tstop  = find(tidedata.SerialDay >= stopday,1,'first');
Tstart = find(time <= startday,1,'last');
Tstop  = find(time >= stopday,1,'first');

% interpolate SWH and tidelevel to model timesteps
wavey = interp1(wavetime(wstart:wstop),...
    wavedata.p1.swh(wstart:wstop),...
    time(Tstart:Tstop));
eta   = interp1(tidedata.SerialDay(tstart:tstop),...
    tidedata.TimeSeries(tstart:tstop),...
    time(Tstart:Tstop));

% check if mean wave period is available
if isfield(wavedata.p1,'mwp')
    period = interp1(wavedata.time(wstart:wstop),...
        wavedata.p1.mwp(wstart:wstop),...
        time(Tstart:Tstop));
else
    % if not, use fixed period of 10s
    period = 10;
end
% calculate mass transport term
V_w = WaveMassTrans(eta,wavey,period,H_out,opt.epsilon,opt.hbt);


% start and stop indices for integration
[u, zeta, terms] = xxl_spacetime_mainloop(u,zeta,eta,V_w,B,...
    opt.dx,opt.dt,H_in,...
    opt.r,opt.lambda,opt.advection,...
    opt.gbt);



% Define output struct
out = struct('u',u, ...
    'zeta',zeta, ...
    'eta', eta, ...
    'time',time, ...
    'B',B,...
    'grid_x',grid_x,...
    'terms',terms);


end
