function [u, zeta, terms] = xxl_spacetime_mainloop(u,zeta,eta,V,B,dx,dt,H_in,r,lambda,advection,gbt)
% Forward integration of u and zeta.
%
% See also XXL_SPACETIME_REALDATA, XXL_SPACETIME_MODEL

% get size of matrices
J = size(u,1);
N = size(u,2) - 1;


gbt = repmat(gbt(:)',J,1);
% x-dependent gbt
% addon = zeros(size(gbt));
% 
% x = linspace(25,1700,J);
% vertices = [0,700,1100,1300,1400,1500,1700];
% values = [0, -0.8, -0.8, -0.1, -0.2, -1, -1];
% 
% start = 1;
% for j = 1:length(vertices)-1
%     [~,stop] = min(abs(x-vertices(j+1)));
% 
%     a = diff(values(j:j+1))/diff(vertices(j:j+1));
%     a0 = values(j);
% 
%     addon(start:stop,1) = a0 + a.*(x(start:stop) - x(start));
% 
%     start = stop + 1;
% end

% addon(:,2) = addon(:,1);
% addon = addon*0.5; 


% gbt = addon + gbt;


R = 1./(1 + dt.*r);

FF = NaN(J,N);
volflux = NaN(J,N);
pressg = NaN(J,N);
advec = NaN(J,N);
F = NaN(J,1);

% two cases: with or without advection terms in momentum equation
switch advection
    
    % first case: with advection terms
    case 'yes'
        
        if nargout > 2
            disp('Saving terms')
            
            
            for n = 1:N
                BB = B;
                for k = 0:7
                    BB(end-k) = BB(end-k) + (eta(n)+1) * (20 - 2*k);
                end
                
                F = V_sld(eta(n),zeta(:,n),gbt,lambda,F);
                FF(:,n) = F;
                volflux(:,n) = volumeflux(eta(n),zeta(:,n),H_in,BB,u(:,n),dx,J);
                
                zeta(:,n+1) = zeta(:,n) + dt./BB(1:2:end).*( V(n) + F(:) - ...
                    volflux(:,n));
                
                
                advec(:,n) = advectionterm(u(:,n),dt,dx,J);
                pressg(:,n) = pressgrad(zeta(:,n+1),eta(n+1),dt,dx,J);
                
                u(:,n+1) = R.* ( u(:,n) - advec(:,n) - pressg(:,n));
                
            end
            terms = struct(...
                'V_w', V, ...
                'V_sld', FF, ...
                'press_grad', pressg, ...
                'advection', advec, ...
                'volflux',volflux ...
                );
        else
            for n = 1:N
                BB = B;
                for k = 0:7
                    BB(end-k) = BB(end-k) + (eta(n)+1) * (20 - 2*k);
                end
                % calculate backflow F
                F = V_sld(eta(n),zeta(:,n),gbt,lambda,F);
                
                
                zeta(:,n+1) = zeta(:,n) + dt./BB(1:2:end).*( V(n) + F(:) - ...
                    volumeflux(eta(n),zeta(:,n),H_in,BB,u(:,n),dx,J));
                
                u(:,n+1) = R .* ( u(:,n) - ...
                    pressgrad(zeta(:,n+1),eta(n+1),dt,dx,J) - ...
                    advectionterm(u(:,n),dt,dx,J));
                
                
            end
        end
        
end


end

function vf = volumeflux(Eta,Zeta,H_in,B,U,dx,J)
% calculate d/dx([zeta+H_in]Bu)
vf = NaN(J,1);
vf(1) = ((Zeta(1) + Zeta(2)).*0.5 + H_in).*B(2).*U(1)./dx;
vf(2:J-1) = (((Zeta(3:J) + Zeta(2:J-1))  .*0.5 + H_in).* B(4:2:end-2) .* U(2:J-1) - ...
    ((Zeta(2:J-1) + Zeta(1:J-2)).*0.5 + H_in).* B(2:2:end-4) .* U(1:J-2)...
    )./dx;
vf(J) = (((Zeta(end) + Eta)       *0.5 + H_in) .*B(end)   *U(end) - ...
    ((Zeta(end) + Zeta(end-1))*0.5 + H_in) .*B(end-2) *U(end-1) ...
    )./dx;

vf = vf(:);
end

function F = V_sld(Eta,Zeta,gbt,lambda,F)
% calculate V_sld
% zmid = (Eta+Zeta)*0.5;
zmid = max(Eta,Zeta);

for k = 1:length(F)
    if zmid(k) < gbt(k,1)
        F(k,1) = 0;
    elseif zmid(k) > gbt(k,2)
%         F(k,1) = 1;
        F(k,1) = 1 + ((zmid(k)-gbt(k,2))./(gbt(k,2)-gbt(k,1)));
    else
        F(k,1) = ((zmid(k)-gbt(k,1))./(gbt(k,2)-gbt(k,1))).^2;
    end
end

F = F.*(Eta-Zeta).*lambda;
F = F(:);
end

function pressg = pressgrad(Zeta,Eta,dt,dx,J)
% calculate pressure gradient term
pressg = NaN(J,1);
g = 9.81;
pressg(1) = g*dt*(Zeta(2) - Zeta(1))./dx;
pressg(2:J-1) = g*dt*(Zeta(3:J) - Zeta(2:J-1))./dx;
pressg(end) = g*dt*(Eta - Zeta(end))/dx;
pressg = pressg(:);
end

function advec = advectionterm(U,dt,dx,J)
% calculate advection term
advec=NaN(J,1);
advec(1) = dt*U(1)*U(2)/(2*dx);
advec(2:J-1) = dt*U(2:J-1).*(U(3:J) - U(1:J-2))./(2*dx);
advec(J) = dt*U(end)*(U(end) - U(end-1))./dx;

advec = advec(:);
end


