function [eta,Hs,T,etatime, time, realTime, dataStartTime] = padetahs(data,dt)
% Calculates a sine wave used for padding the measured time series of sea
% level. Returns padded time series of eta, Hs, T, and some time vectors.
% Hs and T are padded with a constant value equal to the first measured
% value, so the length is equal to the padded eta.
% 
%   [eta,Hs,T,etatime, time, realTime, dataStartTime] = PADETAHS(DATA,DT)
% 
%  DT is model time step.
% 
% Output:
%  -- ETA: padded time series of sea surface height
%  -- HS: padded time series of significant wave height
%  -- T: padded time series of wave period
%  -- ETATIME: time vector corresponding to ETA. In seconds, starting at 0, 
%     step similar to measurements.
%  -- TIME: time vector covering same range as ETATIME, but step is DT.
%  -- realTime: DATENUM time vector covering entire padded time series
%  -- dataStartTime: index in TIME where measurements start.
% 
% See also XXL_SPACETIME_REALDATA, XXL_0D


% "Pad" measured time series of outside SSH with a sine curve.
% Do this to let model stabilize, first period used to have some
% instabilities.

% outsideSSH looks like a sine curve shifted slightly to the right. 
% find how long time it takes for it to cross zero
zerocross = find(data.meansealevelOutside(1:end-1)<0&data.meansealevelOutside(2:end)>0,2);
% detail: interpolate to find precise time
zerotime  = interp1(data.meansealevelOutside(zerocross(1):zerocross(1)+1), ...
                    data.outsideTime(zerocross(1):zerocross(1)+1),0);

% calculate timeshift in seconds
timeshift = (zerotime - data.outsideTime(1))*86400;

% calculate the period of measured SSH oscillation
period = 3600*24*(data.outsideTime(zerocross(2)) - data.outsideTime(zerocross(1)));

% find timestep of measurements
datatimestep = diff(data.outsideTime(1:2))*86400;

% approximate angular frequency of eta
f = 2.*pi/(period); 

% time vector for "padding" sine curve
paddingtime = 0:datatimestep:period;
% calculate sine curve used for padding
etapadding  = max(data.meansealevelOutside)*sin(f*(paddingtime - timeshift));

% merge with measured SSH.
eta = [etapadding(:);data.meansealevelOutside(:)]';

% calculate total time of eta, with padding
totaltime = (length(eta) - 1)*datatimestep;

% create time vector for model
time = 0:dt:totaltime;

% define timevector for eta, with padding
etatime = 0:datatimestep:totaltime;


% find start time of padded time series
padStartTime = data.outsideTime(1) - datenum(0,0,0,0,0,length(paddingtime)*datatimestep);

% create a datenum-vector
realTime = datenum(0,0,0,0,0,time) + padStartTime;

% find where the measured data start in the time vector
dataStartTime = find(realTime <= data.outsideTime(1),1,'last');


% pad Hs-series
Hs = [ones(length(paddingtime),1)*data.outsideHs(1); data.outsideHs(:)]';

% pad period
T = [ones(length(paddingtime),1)*data.outsideT(1); data.outsideT]';

end
