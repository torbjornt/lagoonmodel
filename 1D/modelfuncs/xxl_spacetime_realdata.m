function [out, opt] = xxl_spacetime_realdata(data,varargin)
%
% Xai-Xai lagoon model with space dependency
% 
%    OUT = XXL_SPACETIME_REALDATA(DATA);
%
% first dimension of data matrices:  space
% second dimension of data matrices: time
%
% DATA is a struct with data from the 2012 measurements in Xai-Xai.
% Struct fields should be as in Kai's makePlots.m script, i.e.
%  - meansealevelOutside
%  - outsideTime
%  - outsideHs
%  - outsideT
% 
% The remaining arguments are optional, given with PARAMETER,VALUE pairs.
% They are:
%
%  - dx         Grid distance, default 50
%  - dt         Time step, default 2
%  - r          Friction coefficient, default 0.002
%  - epsilon    Coefficient related to wave mass transport, default 1
%  - bform      Shape of lagoon, see LAGOONWIDTH.M, default 'real'
%  - lambda     Coefficient related to mass flux due to eta-zeta
%               difference: lambda*(eta - zeta). Default 1.
%  - advection  Switch to determine if advection terms should be included in the
%               the momentum equation. Value is 'yes' or 'no', default 'yes'.
%  - hs         Significant wave height. By default, model uses Hs from
%               measurements. If a number is specified, use constant wave
%               height. If a vector is specified, it must be the same
%               length as data.outsideHs.
%  - hbt        two-element vector with upper and lower limit for level of
%               inflow (above upper => max inflow, below lower =>
%               zero). Default [-0.5, 0.3].
%  - gbt        two-element vector with upper and lower limit for level of
%               pressure driven cross reef flow (above upper => max inflow, below lower =>
%               zero) Default defined in XXL_SPACETIME_MAINLOOP.
% 
% Output is two structs, OUT and OPT, the first containing the model results,
% the second containing the input parameters. OUT contains the following
% fields:
%  - u          matrix of the the current speed u
%  - zeta       matrix of the interior surface elevation, zeta
%  - eta        matrix of the exterior surface elevation, eta
%  - B          2*Jx1 vector of lagoon width
%  - grid_x     2*Jx1 vector of x-positions
%  - time       vector for the time of u/zeta/eta
%  - V          vector with wave forcing (first output of WAVEMASSTRANSPORT)
%  - s          vector with integrated Stoke's drift (second output of
%               WAVEMASSTRANSPORT)
%
% Examples:
%     OUT = XXL_SPACETIME_REALDATA(data);
%   Runs model with default parameters, saves result in OUT.
%
%     [OUT,OPT] = XXL_SPACETIME_REALDATA(data,'dx',25);
%   Changes grid distance to 25 m, rather than the default 50. Saves
%   results in OUT, input params in OPT.
% 
% See also XXL_SPACETIME_MODEL, COMPAREDATAMODEL, ETAZETAMOV,
% XXL_SPACETIME_MAINLOOP, WAVEMASSTRANS


% START OF INPUT HANDLING
% http://stackoverflow.com/a/2776238/551171
% set default values
opt = struct('dx',        50,        ...
             'dt',        2,         ...
             'r',         0.002,     ...
             'epsilon',   1,         ...
             'lambda',    1,         ...
             'bform',     'real',    ...
             'advection', 'yes',     ...
             'hbt',       [-0.5 .3], ...
             'gbt',       [-0.1, 1], ...
             'hs',        '');

% list names of possible input arguments
optionNames = fieldnames(opt);
% number of arguments
nArgs = length(varargin);
% check of prop/value pairs is given
if mod(nArgs,2) ~= 0
    error(['Must have parameter/value pairs, ',...
        'you have used an odd number of arguments.'])
end

% Iterate over varargin, overwriting any default arguments
for pair = reshape(varargin,2,[])
    % convert to lowercase
    inpName = lower(pair{1});
    
    % check if argument exists, overwrite
    if any(strmatch(inpName,optionNames))
        opt.(inpName) = pair{2};
    else
        warning('%s is not a recognized parameter, ignored',inpName)
    end
end
% END OF INPUT HANDLING

% length of lagoon
L = 1700;        

% number of grid points
J  = floor(L/opt.dx);


% width of lagoon
[B, grid_x] = lagoonwidth(opt.bform,J,L,opt.dx);
% mean water depth inside
H_in = mean(data.insideSSH);
% mean water depth outside
H_out = mean(data.outsideSSH);



% handle significant waveheight
if isnumeric(opt.hs)
    if min(size(opt.hs)) > 1
        errMess = ['hs must be a number or a vector, I do not know what ',...
            'to do with a matrix.\nHence, no results. Change your input and rerun'];
        error('ErrorTests:convertTest',errMess)
    end
    if length(opt.hs) == 1
        data.outsideHs = ones(size(data.outsideHs))*opt.hs;
    elseif length(opt.hs) == length(data.outsideHs)
        data.outsideHs = reshape(opt.hs,size(data.outsideHs));
    else
        errMess = [...
            'The specified hs does not have the same length as the data,\n',...
            'so I do not know what to do. Change your input and rerun\n',...
            'length(data.outsideHs) = ', num2str(length(data.outsideHs)), ...
            '\nlength(hs) = ', num2str(length(opt.hs))];
        error('ErrorTests:convertTest',errMess)
    end
end
        

% pad the measured eta (SSH) with a sine curve, because of instabilities at
% start of model runs
[eta,padded_Hs,padded_period,etatime, time, realTime, dataStartTime] = padetahs(data,opt.dt);

% calculate number of (new) timesteps needed to reach end of measurements
N = length(time) - 1;

% calculate mass transport term
[V_w,stoke] = WaveMassTrans(eta,padded_Hs,padded_period',H_out,opt.epsilon,opt.hbt);
% [V_w,stoke] = WaveMassTransG(eta,padded_Hs,padded_period,-.4,opt.epsilon,opt.hbt);

% interpolate eta and V to timesteps defined by time vector
eta = interp1(etatime,eta,time,'spline');
V = interp1(etatime,V_w,time,'spline');


% initalize matrices for interior water level (zeta) and
% current speed in lagoon (u). zeta(t=0) = 0, u(t=0) = 0.
zeta = ones(J,N+1) * eta(1);
u    = zeros(J,N+1);



[u, zeta, terms] = xxl_spacetime_mainloop(u,zeta,eta,V,B,...
                                   opt.dx,opt.dt,H_in,...
                                   opt.r,opt.lambda,opt.advection,...
                                   opt.gbt);

% remove the padding, just the measured time period
u    = u(:,dataStartTime:end);
zeta = zeta(:,dataStartTime:end);
eta  = eta(:,dataStartTime:end);
time = realTime(:,dataStartTime:end);
V = V(dataStartTime:end);
% FF = FF(:,dataStartTime:end);

% Define output struct
out = struct('u',u, ...
             'zeta',zeta, ...
             'eta', eta, ...
             'B', B, ...
             'grid_x',grid_x, ...
             'time',time,...
             'Vint', V,...
             'V',V_w,...
             's',stoke,...
             'terms',terms);
end

