function out = xxl_spacetime_groups(opt,group_period)
%
% Xai-Xai lagoon model with space dependency
%
% first dimension of data matrices:  space
% second dimension of data matrices: time
%
%    out = XXL_SPACETIME_WAVEGROUP(OPT)
%  OPT is a struct from e.g. XXL_SPACETIME_MODEL
%
% Output is a struct containing the following fields:
%  - u         JxN matrix of the the current speed u
%  - zeta      JxN matrix of the interior surface elevation, zeta
%  - eta       JxN matrix of the exterior surface elevation, eta
%  - B         2*Jx1 vector of lagoon width
%  - grid_x    2*Jx1 vector of x-positions
%  - time      1xN time vector, time in minutes.
%
% 
% See also XXL_SPACETIME_MODEL, COMPAREDATAMODEL, ETAZETAMOV,
% XXL_SPACETIME_MAINLOOP


% some  constants
% timesteps -- run model for 10 hours
hr     = 3;
N      = hr*3600/opt.dt;
% length of lagoon
L      = 1700;
% number of grid points
J      = floor(L/opt.dx);
% mean water depth inside
H_in   = 3;
% typical period for waves
period = 10;
% depth outside reef
depth  = 17;
% set hs, if not in opt
if isempty(opt.hs)
    opt.hs = 1.5;
end
% default group period
if nargin < 2
    group_period = 60;
end


% width of lagoon
[B grid_x] = lagoonwidth('simple',J,L,opt.dx);


% time vector
time = 0:opt.dt:hr*3600;
% SSH outside lagoon, constant at 0.5m above mean
eta  = 0.5*ones(size(time));

% periodic time series for Hs, to emulate wave groups
% period for "wave groups", 1 minute
% Hs = ones(size(eta))*opt.hs + 0.1*opt.hs*sin(time*2*pi/group_period);
% Hs = ones(size(eta))*opt.hs + 0.1*opt.hs*sin(time*2*pi/(group_period/5)) + 0.1*opt.hs*sin(time*2*pi/(group_period/6));
Hs = ones(size(eta))*opt.hs + 0.1*opt.hs*sin(time*2*pi/10) + 0.1*opt.hs*sin(time*2*pi/group_period);
 
% % mass transport based on H_s
V = WaveMassTrans(eta,...
                  Hs,...
                  ones(size(eta))*period,...
                  depth,...
                  opt.epsilon,...
                  opt.hbt);


% initalize matrices for interior water level (zeta) and
% current speed in lagoon (u). zeta(t=0) = 0, u(t=0) = 0.
zeta = zeros(J,N+1);
u    = zeros(J,N+1);


% u(n+1) coefficient
R = 1/(1 + opt.r*opt.dt);



[u, zeta] = xxl_spacetime_mainloop(u,zeta,eta,V,B,...
                                   opt.dx,opt.dt,H_in,...
                                   R,opt.lambda,opt.advection);

                               

% Define output struct
out = struct('u',u, ...
             'zeta',zeta, ...
             'eta', eta, ...
             'B', B, ...
             'grid_x',grid_x,...
             'time',time/60,...
             'V',V,...
             'Hs',Hs);
end
