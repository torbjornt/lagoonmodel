function [V,V_w,V_simp] = WaveMassTrans(eta,Hs,T,depth,eps,hbt,oldornew,const)
% Calculate a time series of mass influx, given time series of eta and
% H_s, the depth, epsilon parameter and upper/lower level of linear
% increase in flux. Below, flux is zero, above, flux is 1.
% 
%   [V,V_w] = WaveMassTrans(eta,Hs,T,depth,eps,hbt,oldornew)

if nargin < 7 || isempty(oldornew)
    oldornew = 'new';
end

if nargin < 8
    const = 0;
end

if length(Hs) == 1
    Hs = ones(size(eta)) * Hs;
end

if length(T) == 1
    T = ones(size(eta)) * T;
end


% lower and upper limit
hbt = sort(hbt);
g = 9.81;

% calculate wave mass transport based on Longuet-Higgins (1953)
% peak period of 10 seconds
% f = 2*pi./T;
% wave number
% k = f.^2./g;

z = -depth:0.5:0;

% mass transport based on Hs
% V_w = (0.5.*Hs).^2.*f.*k.*cosh(2.*k.*(-depth))./(2.*sinh(depth.*k).*sinh(depth.*k));
% V_w = - (0.5.*Hs).^2.*f.*0.5.*(exp(-2*k*depth) - exp(0));

[HS,Z]  = meshgrid(Hs,z);
[per,~] = meshgrid(T,z);
F = 2*pi./per;
% K = F.^2./g;

if const
    k = kcalc(T(1));
    [K, ~] = meshgrid(repmat(k,length(T),1),z);
else
    k = kcalc2(T);
    [K, ~] = meshgrid(k,z);
end

V_w = (0.5.*HS).^2.*F.*K.*cosh(-2.*K.*(Z+depth))./(2.*sinh(depth.*K).*sinh(depth.*K));

V_simp = (0.5*HS).^2 .* F .* K .* exp(2.*K.*Z);

switch oldornew
    case 'new'
        V_w = trapz(z,V_w,1);
        V_simp = trapz(z,V_simp,1);
        % linear
        % F = (eta-hbt(1))./(hbt(2)-hbt(1));
        
        F = (eta-hbt(1)).^2./(hbt(2)-hbt(1)).^2;
        F(eta<hbt(1)) = 0;
        F(eta>hbt(2)) = 1;
        % time series for inflow
        V = eps .* V_w(:)' .* F(:)';
    case 'old'
        V = eps.*V_w(end,:).*max(eta-hbt,0);
%         V = eps .* max(5+eta-hbt,0) .* V;
end

end

function wn = kcalc(T)

g = 9.81;
h = 17;
k = 1./(1:0.01:500);
om = (2*pi./T).^2;
omsq = g.*k.*tanh(k.*h);
wn = NaN(size(om));

for l = 1:length(T)
    [~, ind] = min(abs(om(l) - omsq));
    wn(l) = k(ind);
end
end

function wn = kcalc2(T)

wn = NaN(size(T));
T = round(T*10)/10;
unique_T = unique(T);

g = 9.81;
h = 17;
k = 1./(1:0.01:500);
om = (2*pi./unique_T).^2;
omsq = g.*k.*tanh(k.*h);
wn_lookup = NaN(size(om));

for l = 1:length(unique_T)
    [~, ind] = min(abs(om(l) - omsq));
    wn_lookup(l) = k(ind);
end

for l = 1:length(T)
    wn(l) = wn_lookup(unique_T == T(l));
end
end


