All code in here is licensed under the MIT License. A short description of the content is below.

# Numerical models of circulation in lagoon

This repository contains two numerical models describing an alongshore flow inside a lagoon, forced by sea level outside the lagoon and incoming waves.
The models are implemented in MATLAB, and were developed and used for my [master's thesis in physical oceanography](https://bitbucket.org/torbjornt/masterthesis).


## 0D model

The folder `0D` contains a single point model.

 - `xxl_0D.m` is a function that runs a simulation with a set of customisable parameters, and (optionally) plots the results.
 - `runZero.m` is a script that runs a selection of different cases.

## 1D model

The folder `1D` contains a model including one spatial dimension as well as time.

 - the `auxiliary` folder contains a lot of junk that I should remove
 - the `GUI` folder contains the very quick-and-dirty code for a GUI that hasn't been updated for a long time, and probably doesn't work properly anymore.
 - the `modelfuncs` folder contain the actual model.
   Functions:
 
      - `xxl_spacetime_mainloop.m`: the actual time integration
      - `xxl_spacetime_model.m`: for running an ideal case
      - `xxl_spacetime_realdata.m`: for running a case with measured data as forcing
      - `xxl_spacetime_climatology.m`: for running a two-year long case with model data as forcing
      - `xxl_spacetime_dates.m`: similar to previous, but just for a specified period
      - `WaveMassTrans.m`: for calculating cross reef mass transport caused by waves.
      - `padetahs.`: pads measured time series a bit
      - `lagoonwidth.m`: returns width of model lagoon for different shapes
      - Ignore the rest.

The `runX.m` scripts are for different cases:

 - `runModel.m` uses `xxl_spacetime_model.m` to run an ideal case.
 - `runRealdata.m` uses `xxl_spacetime_realdata.m` to run a case with measured data as input
 - `runClimate.m` uses `xxl_spacetime_climatology.m` to run a two-year case.

Ignore the rest.