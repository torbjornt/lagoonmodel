function [u,zeta,eta,time,uterms,zterms,V,params] = xxl_0d(varargin)
% Xai-Xai Lagoon Circulation
% 0D-model ,forward-backward scheme
% 
% Inputs are optional, PARAMETER,VALUE pairs:
%  - alfa: pressure gradient coefficient
%  - gamma: friction coefficient
%  - epsilon: wave mass flux coefficient
%  - dt: time step
%  - href: lower limit of eta for flux into the lagoon
%  - data: use measurements from 2012 as forcings (yes or no)
%  - plot: create a plot of results (yes or no)
% 
% Outputs are:
%  - u: timeseries of modeled current speed
%  - zeta: timeseries of modeled sea level inside lagoon
%  - eta: timeseries of sea level outside lagoon
%  - time: time vector
%  - uterms: the separate terms of u-equation
%  - zterms: the separate terms of zeta-equation
%  - V: mass flux timeseries
%  - params: Struct with the model parameters.
% 
%   [U,Z] = XXL_0D('dt',30,'plot','no');
%  changes time step to 30 seconds, don't create a plot. 


% define defaults
% dimensionless pressure grad coefficient
alfa = 10;

% dimensionless friction coefficient  
gamma = 10;

% default coefficient for wave flux
epsilon = 0.25;

% default timestep
dt = 60;

% reference water level href: if eta > href => inflow
href = -0.5;

% use data or not
data = 'no';

% create plot
plotornot = 'yes';

% width of opening
b = 20;

% old or new V
oldornew = 'old';

% check varargin for new definitions, overwrite.
for val = 1:length(varargin)-1;
    if strcmp(varargin{val},'dt')
        dt = varargin{val+1};
    elseif strcmp(varargin{val},'epsilon')
        epsilon = varargin{val+1};
    elseif strcmp(varargin{val},'gamma')
        gamma = varargin{val+1};
    elseif strcmp(varargin{val},'alfa')
        alfa = varargin{val+1};
    elseif strcmp(varargin{val},'href')
        href = varargin{val+1};
    elseif strcmp(varargin{val},'data')
        data = varargin{val+1};
    elseif strcmp(varargin{val},'plot')
        plotornot = varargin{val+1};
    elseif strcmp(varargin{val},'b')
        b = varargin{val+1};
    elseif strcmp(varargin{val},'oldornew')
        oldornew = varargin{val+1};
    end
end

% M2 tidal period
T_M2 = 12.417*3600;
% gravity
g = 9.81; 

% geometry
B = 150;  % width
L = 1700; % length
d = 3;    % depth

% Two cases -- with or without measurements as input.
switch data
    case 'no'
        f  = 2*pi/T_M2;          % angular frequency
        time = 0:dt/2:3*T_M2+dt/2;      % time vector
        
        % exterior water level (for simplicity, simple sine curve)
        eta = sin(f.*time);
        
        Hs = ones(size(eta))*1.5;
        per = ones(size(eta))*10;
        
        % mass transport term
%         V = 0.15*ones(size(time)); % mass transport waves
%         VV = epsilon .* max(5+eta-href,0) .* V;
        V = WaveMassTrans(eta,Hs,per,17,epsilon,href,oldornew);
   case 'yes'
        load 2012-concurrent_data
        [eta,Hs,per,etatime, time, ~, ~] = padetahs(data,dt);
        V_w = WaveMassTrans(eta,Hs,per,17,epsilon,href,oldornew);
        
        time = 0:dt/2:time(end)+dt/2;
        % interpolate
        eta = interp1(etatime,eta,time,'spline');
        V = interp1(etatime,V_w,time,'spline');
%         V = epsilon * max(eta-href,0) * V;
end


% initalize vectors for interior water level (zeta) and
% current speed in lagoon (u). zeta(t=0) = eta(t=0).
zeta = ones(length(time)/2,1)*eta(1);
u = zeros(length(time)/2,1);

% initialize matrices for the different terms
uterms = NaN(length(time)/2-1,3);
zterms = NaN(length(time)/2-1,3);


% solve equation set with forward-backward scheme
for n = 1:length(time)/2-1
    zeta(n+1) = zeta(n) + dt/B*(...
         V(2*n) ...
        - b/L * (d+zeta(n))*u(n) ...
    );
    
    u(n+1) = u(n) + dt*alfa*g/L*(zeta(n+1)-eta(2*n+1)) ...
        - dt*gamma/(T_M2*0.5)*u(n);
    
    % save the different terms separately
%     uterms(n,1) = u(n);
%     uterms(n,2) = dt*alfa*g/L*(zeta(n+1)-eta(2*n+1));
%     uterms(n,3) = dt*gamma/(T*0.5)*u(n);
%     
%     zterms(n,1) = zeta(n);
%     zterms(n,2) = epsilon * max(eta(2*n)-href,0) * V(2*n);
%     zterms(n,3) = b/L * (d+zeta(n))*u(n);
end

% plot?
switch plotornot
    case 'yes'
        % plot results with time in hours
        figure;
        ax = plotyy(time(1:2:end)./3600,zeta,time(2:2:end)./3600,u);
        xlabel('hours')
        set(get(ax(1),'ylabel'),'string','SSH [m]')
        set(get(ax(2),'ylabel'),'string','Current speed [m/s]',...
            'rotation',-90,'verticalalignment','baseline')
        set(ax(1),'ylim',[-1.5,1.5],'ytick',-1:.5:1.4)
        set(ax(2),'ylim',[-1.5,1.5],'ytick',-1:.5:1.4)
        hold on
        plot(ax(1),time./3600,eta,'-.g')
        legend('zeta','eta','u')
    case 'no'
        % do nothing
end

params = struct('gamma',gamma,'alfa',alfa,'epsilon',epsilon,...
    'href',href,'b',b,'dt',dt,'B',B,'L',L);

end
