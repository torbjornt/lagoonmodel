% Define different cases for 0D model, and a switch 
% to ... switch between them.

kasus = 4;

switch kasus
  case 1
    b = 20;
    alfa = 10;
    gamma = 10;
    epsilon = .2;
    href = -.5;
  case 2
    b = 50;
    alfa = .7;
    gamma = 10;
    epsilon = .3;
    href = -.5;
  case 3
    b = 100;
    alfa = 1;
    gamma = 10;
    epsilon = 2.8;
    href = -.5;
  case 4
    b = 100;
    alfa = 1;
    gamma = 50;
    epsilon = .5;
    href = [-1.2 .4];
    dt = 60;
end



[ud,zd,ed,td,utd,ztd,Vd,opt0d]= xxl_0d('plot','yes','oldornew','new','data','yes',...
    'alfa',alfa,'epsilon',epsilon,'gamma',gamma,'b',b,'href',href,'dt',dt);

% filename = sprintf('/Home/stud6/tta089/Documents/Master/figures/0D-idealcase%u.dat',kasus);
% fid = fopen(filename,'w');
% fprintf(fid,'utime\tu\tztime\tzeta\teta\n');
% for k = 1:length(ud)
%     fprintf(fid,'%.5f\t%.5f\t%.5f\t%.5f\t%.5f\n',...
%         td(2*k-1)/3600,ud(k),td(2*k)/3600,zd(k),ed(2*k));
% end
% 
% fclose(fid);


%% find maximalag
U = ud(500:1500);
Z = zd(500:1500);

[~,Um] = max(U);
[~,Zm] = max(Z);

topdiff = (Um-Zm) + 0.5; % minutt